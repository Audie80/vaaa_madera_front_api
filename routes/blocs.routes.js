module.exports = (app) => {
    const blocs = require("../controllers/blocs.controller.js");

    // Retrieve all Blocs
    app.get(
      "/api/commercial/blocs",
      blocs.findAll
    );

    // Retrieve all Blocs
    app.get(
      "/api/be/blocs",
      blocs.findAll
    );
  
    //Create a new Bloc
    app.post(
      "/api/be/bloc",
      blocs.create
    );
  
    // Retrieve a single Products with idBloc
    app.get(
      "/api/be/bloc/:idBloc",
      blocs.findOne
    );
    
    // Update an Products with idBloc
    app.put(
      "/api/be/bloc/:idBloc",
      blocs.update
    );
  
    // Delete a Bloc with idBloc
    app.delete(
      "/api/be/bloc/:idBloc",
      blocs.delete
    );
  };
  