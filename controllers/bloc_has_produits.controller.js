const mysql = require("mysql2");

// Database connection
var bddsql = require("../config/database.config");

// TODO donner aussi les blocs du produit
// Retrieve and return all blocs of a product.
exports.findAll = (req, res) => {
    let urlprod = req.params.idProduit;
    let sql =
      "SELECT bloc_has_produit.id, bloc_id, produit_id, quantite "+ 
      "FROM bloc_has_produit "+
      "LEFT JOIN bloc ON bloc_has_produit.bloc_id = bloc.id "+
      "WHERE produit_id =" + mysql.escape(urlprod);
    bddsql.BDDSQL.query(sql, function (err, result) {
      if (err) throw err;
      res.send(result);
    });
  };

  
// Create a new bloc.
exports.create = (req, res) => {
    let urlprod = req.params.idProduit;
    let sql =
      "INSERT INTO bloc_has_produit(bloc_id, produit_id, quantite) VALUES (" +
      mysql.escape(req.body.bloc_id) +
      ", " +
      mysql.escape(urlprod) +
      ", " +
      mysql.escape(req.body.quantite) +
      ")";
    bddsql.BDDSQL.query(sql, function (err, result) {
      if (err) throw err;
    });
  };

// Retrieve a single bloc_has_produit with idBloc
exports.findOne = (req, res) => {
    let url = req.params.idBloc_has_produits;
    let sql =
      "SELECT id, bloc_id, produit_id, quantite "+
      "FROM bloc_has_produit "+
      "WHERE id=" + mysql.escape(url);
    bddsql.BDDSQL.query(sql, function (err, result) {
      if (err) throw err;
      res.send(result);
    });
  };

// Update a bloc_has_produit with idBloc
exports.update = (req, res) => {
    let urlbloc = req.params.idBloc;
    let urlprod = req.params.idProduit;
    let sql =
      "UPDATE bloc_has_produit SET bloc_id=" +
      mysql.escape(urlbloc) +
      ", produit_id=" +
      mysql.escape(urlprod) +
      ", quantite=" +
      mysql.escape(req.body.quantite) +
      " WHERE bloc_has_produit.id=" +
      mysql.escape(urlbloc);
    bddsql.BDDSQL.query(sql, function (err, result) {
      if (err) throw err;
    });
  };
  
// Delete a bloc with idBloc
exports.delete = (req, res) => {
    let url = req.params.idBloc;
    let sql = "DELETE FROM bloc_has_produit WHERE id=" + mysql.escape(url);
    bddsql.BDDSQL.query(sql, function (err, result) {
      if (err) throw err;
    });
  };
  