const mysql = require("mysql2");

// Database connection
var bddsql = require("../config/database.config");

// Retrieve and return all clients from the database.
exports.findAll = (req, res) => {
  let sql = "SELECT id, prenom, nom, adresse FROM client";
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
};

// Create a new client.
exports.create = (req, res) => {
  console.log("client créé");
  let sql =
    "INSERT INTO client(prenom, nom, adresse) VALUES (" +
    mysql.escape(req.body.prenom) +
    "," +
    mysql.escape(req.body.nom) +
    "," +
    mysql.escape(req.body.adresse) +
    ")";
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
  });
};

// Retrieve a single Client with idClient
exports.findOne = (req, res) => {
  let url = req.params.idClient;
  let sql =
    "SELECT id, prenom, nom, adresse FROM client WHERE id=" + mysql.escape(url);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
};

// Update a Client with idClient
exports.update = (req, res) => {
  let url = req.params.idClient;
  let sql =
    "UPDATE client SET prenom=" +
    mysql.escape(req.body.prenom) +
    ", nom=" +
    mysql.escape(req.body.nom) +
    ", adresse=" +
    mysql.escape(req.body.adresse) +
    " WHERE id=" +
    mysql.escape(url);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
  });
};

// TODO : la suppression d'un client ne doit pas le supprimer mais anonymiser tous les champs de données personnelles
// Delete a Client with idClient
// exports.delete = (req, res) => {
//   let url = req.params.idClient;
//   let sql = "DELETE FROM client WHERE id=" + mysql.escape(url);
//   bddsql.BDDSQL.query(sql, function (err, result) {
//     if (err) throw err;
//   });
// };

// Retrieve and return last created client id.
exports.findLastOne = (req, res) => {
  let sql = "SELECT MAX(id) as id FROM client";
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
};
