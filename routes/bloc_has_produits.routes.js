module.exports = (app) => {
    const blocs = require("../controllers/bloc_has_produits.controller.js");
  
    // Retrieve all Blocs_has_produit
    app.get(
      "/api/commercial/clients/:idClient/projets/:idProjet/produits/:idProduit/bloc_has_produits",
      blocs.findAll
    );
  
    //Create a new Bloc
    app.post(
      "/api/commercial/clients/:idClient/projets/:idProjet/produits/:idProduit/bloc_has_produit",
      blocs.create
    );
  
    // Retrieve a single Products with idBloc
    app.get(
      "/api/commercial/clients/:idClient/projets/:idProjet/produits/:idProduit/bloc_has_produits/:idBloc_has_produits",
      blocs.findOne
    );
    
    // Update an Products with idBloc
    app.put(
      "/api/commercial/clients/:idClient/projet/:idProjet/produit/:idProduit/bloc_has_produits/:idBloc_has_produits",
      blocs.update
    );
  
    // Delete a Bloc with idBloc
    app.delete(
      "/api/commercial/clients/:idClient/projet/:idProjet/produit/:idProduit/bloc_has_produits/:idBloc_has_produits",
      blocs.delete
    );
  };
  