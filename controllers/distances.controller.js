const mysql = require("mysql2");

// Database connection
var bddsql = require("../config/database.config");

// Retrieve and return all distances.
exports.findAll = (req, res) => {
  let sql = "SELECT id, kilometrage, prix FROM distance";
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
};
