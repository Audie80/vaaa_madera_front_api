module.exports = (app) => {
  const projets = require("../controllers/projets.controller.js");

  // Retrieve all Projects
  app.get("/api/commercial/clients/:idClient/projets", projets.findAll);

  //Create a new Project
  app.post("/api/commercial/clients/:idClient/projet", projets.create);

  // Retrieve a single Project with idProjet
  app.get(
    "/api/commercial/clients/:idClient/projets/:idProjet",
    projets.findOne
  );

  // Retrieve all products prices of a Project
  app.get(
    "/api/commercial/clients/:idClient/projets/:idProjet/prix",
    projets.findPrices
  );

  // Update an Project with idProjet
  app.put(
    "/api/commercial/clients/:idClient/projets/:idProjet",
    projets.update
  );

  // Delete an Project with idProjet
  app.delete(
    "/api/commercial/clients/:idClient/projets/:idProjet",
    projets.delete
  );

  // Find last created Project
  app.get("/api/commercial/lastproject", projets.findLastOne);
};
