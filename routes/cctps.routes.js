module.exports = (app) => {
  const cctps = require("../controllers/cctps.controller.js");

  // Retrieve all CCTPs
  app.get("/api/cctps", cctps.findAll);
};
