const mysql = require("mysql2");

// Database connection
var bddsql = require("../config/database.config");

// Retrieve and return all products of a project.
exports.findAll = (req, res) => {
  let urlproj = req.params.idProjet;
  let sql =
    "SELECT produit.id, produit.nom, produit.gamme_id, gamme.nom as gamme " +
    "FROM produit " +
    "LEFT JOIN gamme ON gamme.id = produit.gamme_id " +
    "WHERE projet_id =" +
    mysql.escape(urlproj);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
};

// Create a new product.
exports.create = (req, res) => {
  console.log("produit créé");
  let urlproj = req.params.idProjet;
  let sql =
    "INSERT INTO produit(nom, gamme_id, projet_id) VALUES (" +
    mysql.escape(req.body.nom) +
    ", " +
    mysql.escape(req.body.gamme) +
    ", " +
    mysql.escape(urlproj) +
    ")";
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
  });
};

// Retrieve a single product with idProduit
exports.findOne = (req, res) => {
  let url = req.params.idProduit;
  let sql =
    "SELECT produit.nom, produit.gamme_id, gamme.nom as gamme " +
    "FROM produit " +
    "LEFT JOIN gamme ON gamme.id = produit.gamme_id " +
    "WHERE produit.id =" +
    mysql.escape(url);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
};

// Retrieve and return all blocs prices of a product.
exports.findPrices = (req, res) => {
  let urlprod = req.params.idProduit;
  let sql =
    "SELECT SUM(bloc.prix_assemblage * bloc_has_produit.quantite) as price " +
    "FROM bloc_has_produit " +
    "LEFT JOIN bloc ON bloc.id = bloc_has_produit.bloc_id " +
    "WHERE produit_id =" +
    mysql.escape(urlprod);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
};

// Update a product with idProduit
exports.update = (req, res) => {
  let url = req.params.idProduit;
  let sql =
    "UPDATE produit SET nom=" +
    mysql.escape(req.body.nom) +
    " WHERE id=" +
    mysql.escape(url);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
  });
};

// Delete a product with idProduit
exports.delete = (req, res) => {
  console.log("produit supprimé");
  let url = req.params.idProduit;
  let sql = "DELETE FROM produit WHERE id=" + mysql.escape(url);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
  });
};

// Retrieve and return last created product id.
exports.findLastOne = (req, res) => {
  let sql = "SELECT MAX(id) as id FROM produit";
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
};
