module.exports = (app) => {
  const produits = require("../controllers/produits.controller.js");

  // Retrieve all Products
  app.get(
    "/api/commercial/clients/:idClient/projets/:idProjet/produits",
    produits.findAll
  );

  //Create a new Product
  app.post(
    "/api/commercial/clients/:idClient/projets/:idProjet/produit",
    produits.create
  );

  // Retrieve a single Products with idProduit
  app.get(
    "/api/commercial/clients/:idClient/projets/:idProjet/produits/:idProduit",
    produits.findOne
  );

  // Retrieve all blocs prices of a Product
  app.get(
    "/api/commercial/clients/:idClient/projets/:idProjet/produits/:idProduit/prix",
    produits.findPrices
  );

  // Update an Products with idProduit
  app.put(
    "/api/commercial/clients/:idClient/projets/:idProjet/produits/:idProduit",
    produits.update
  );

  // Delete an Products with idProduit
  app.delete(
    "/api/commercial/clients/:idClient/projets/:idProjet/produits/:idProduit",
    produits.delete
  );

  // Find last created Product
  app.get("/api/commercial/lastproduct", produits.findLastOne);
};
