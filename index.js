const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const clientsRoutes = require("./routes/clients.routes");
const projetsRoutes = require("./routes/projets.routes");
const produitsRoutes = require("./routes/produits.routes");
const blocsRoutes = require("./routes/blocs.routes");
const blocsHasProduitsRoutes = require("./routes/bloc_has_produits.routes");
const distancesRoutes = require("./routes/distances.routes");
const gammesRoutes = require("./routes/gammes.routes");
const cctpsRoutes = require("./routes/cctps.routes");

// create express app
const app = express();

//Définition du port de l'application. Par défaut : port 3000.
const port = process.env.PORT || 3000;

// Database connection
var bddsql = require("./config/database.config");
var message = "Successfully connected to the database.";
bddsql.BDDSQL.connect(function (err) {
  if (err) throw err;
  console.log(message);
});

// permet de poster des objets JSON
app.use(express.json());

// enable CORS
app.use(cors());

// parse requests of content-type - application/json
app.use(bodyParser.json());

//Démarrage de l'API
clientsRoutes(app);
projetsRoutes(app);
produitsRoutes(app);
blocsRoutes(app);
blocsHasProduitsRoutes(app);
distancesRoutes(app);
gammesRoutes(app);
cctpsRoutes(app);

//Démarrage du serveur
app.listen(port, () => {
  console.log(`Server is running on port: ${port} `);
});

module.exports = app;

// const express = require( 'express' );
// const cors = require('cors');
// const bodyParser = require('body-parser');
// const morgan = require('morgan');

// const app = express();

// app.use(morgan('tiny'));
// app.use(cors());
// app.use(bodyParser.json());

app.get("/api/commercial/blocs", function (req, res) {
  var blocsList = require("./json/blocs.json");
  res.json(blocsList);
});
