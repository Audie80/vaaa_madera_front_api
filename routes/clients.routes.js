module.exports = (app) => {
  const clients = require("../controllers/clients.controller.js");

  // Retrieve all Clients
  app.get("/api/commercial/clients", clients.findAll);

  //Create a new Client
  app.post("/api/commercial/client", clients.create);

  // Retrieve a single Client with idClient
  app.get("/api/commercial/clients/:idClient", clients.findOne);

  // Update an Client with idClient
  app.put("/api/commercial/clients/:idClient", clients.update);

  // Delete an Client with idClient
  // app.delete("/api/commercial/clients/:idClient", clients.delete);

  // Find last created Client
  app.get("/api/commercial/lastclient", clients.findLastOne);
};
