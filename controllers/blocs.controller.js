const mysql = require("mysql2");

// Database connection
var bddsql = require("../config/database.config");

// Retrieve and return all blocs.
exports.findAll = (req, res) => {
  let sql =
    "SELECT bloc.id, bloc.nom, bloc.prix_assemblage, bloc.dimension_id, dimension.unite " +
    "FROM bloc " +
    "LEFT JOIN dimension ON bloc.dimension_id = dimension.id ";
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
};

/* // Retrieve and return all blocs of a product.
exports.findAll = (req, res) => {
  let urlprod = req.params.idProduit;
  let sql =
    "SELECT bloc.id, bloc_has_produit.quantite, bloc.nom, bloc.prix_assemblage, dimension.unite " +
    "FROM bloc_has_produit " +
    "LEFT JOIN bloc ON bloc_has_produit.bloc_id = bloc.id " +
    "LEFT JOIN dimension ON bloc.dimension_id = dimension.id " +
    "WHERE produit_id =" +
    mysql.escape(urlprod);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
}; */

// Create a new bloc.
exports.create = (req, res) => {
  let url = req.params.idBloc;
  let sql =
    "INSERT INTO bloc(nom, prix_assemblage, dimension_id, gamme_id, coupe_principe_id) VALUES ("+
    mysql.escape(req.body.nom) +
    ", " +
    mysql.escape(req.body.prix_assemblage) +
    ", " +
    mysql.escape(req.body.dimension) +
    ", " +
    mysql.escape(req.body.gamme) +
    ", " +
    mysql.escape(req.body.coupe_principe) +
    " WHERE bloc.id=" +
    mysql.escape(url);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
  });
};

// Retrieve a single bloc with idBloc
exports.findOne = (req, res) => {
  let url = req.params.idBloc;
  let sql =
    "SELECT bloc.id, bloc_has_produit.quantite, bloc.nom, bloc.prix_assemblage, dimension.unite, bloc.gamme_id, bloc.coupe_principe_id " +
    "FROM bloc " +
    "LEFT JOIN bloc_has_produit ON bloc_has_produit.bloc_id = bloc.id " +
    "LEFT JOIN dimension ON bloc.dimension_id = dimension.id " +
    "WHERE bloc.id = " +
    mysql.escape(url);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
};

// Update a bloc with idBloc
exports.update = (req, res) => {
  let url = req.params.idBloc;
  let sql =
    "UPDATE bloc SET nom=" +
    mysql.escape(req.body.nom) +
    ", prix_assemblage=" +
    mysql.escape(req.body.prix_assemblage) +
    ", dimension_id=" +
    mysql.escape(req.body.dimension) +
    ", gamme_id=" +
    mysql.escape(req.body.gamme) +
    ", coupe_principe_id=" +
    mysql.escape(req.body.coupe_principe) +
    " WHERE id=" +
    mysql.escape(url);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
  });
};

// Delete a bloc with idBloc
exports.delete = (req, res) => {
  let url = req.params.idBloc;
  let sql = "DELETE FROM bloc WHERE id=" + mysql.escape(url);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
  });
};
