module.exports = (app) => {
    const gammes = require("../controllers/gammes.controller.js");
  
    // Retrieve all Distances
    app.get("/api/gammes", gammes.findAll);
  };
  