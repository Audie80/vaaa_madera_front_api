const mysql = require("mysql2");

// Database connection
var bddsql = require("../config/database.config");

// Retrieve and return all projects of a client.
exports.findAll = (req, res) => {
  let urlproj = req.params.idClient;
  let sql =
    "SELECT id, nom, date, remise, prix_final, adresse, etat_id FROM projet WHERE client_id =" +
    mysql.escape(urlproj) +
    "ORDER BY date DESC";
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
};

// Create a new project.
exports.create = (req, res) => {
  console.log("projet créé");
  let urlproj = req.params.idClient;
  let nowDate = new Date();
  function twoDigits(d) {
    if (0 <= d && d < 10) return "0" + d.toString();
    return d.toString();
  }
  let datetime =
    nowDate.getUTCFullYear() +
    "" +
    twoDigits(nowDate.getUTCMonth() + 1) +
    "" +
    twoDigits(nowDate.getUTCDate() + 1);

  let sql =
    "INSERT INTO projet(nom, date, remise, prix_final, adresse, client_id, marge_id, etat_id, distance_id, CCTP_id) VALUES (" +
    mysql.escape(req.body.nom) +
    "," +
    datetime +
    "," +
    mysql.escape(req.body.remise) +
    "," +
    mysql.escape(req.body.prix) +
    "," +
    mysql.escape(req.body.adresse) +
    "," +
    mysql.escape(urlproj) +
    ", 1, 2," +
    mysql.escape(req.body.distance) +
    "," +
    mysql.escape(req.body.cctp) +
    ")";
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
  });
};

// Retrieve a single project with idProjet
exports.findOne = (req, res) => {
  let url = req.params.idProjet;
  let sql =
    "SELECT id, nom, remise, prix_final, adresse, date, etat_id, distance_id, CCTP_id FROM projet WHERE id=" +
    mysql.escape(url);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
};

// Retrieve and return all products prices of a project.
exports.findPrices = (req, res) => {
  let urlproj = req.params.idProjet;
  let sql =
    "SELECT SUM(bloc.prix_assemblage * bloc_has_produit.quantite) as price " +
    "FROM produit " +
    "LEFT JOIN bloc_has_produit ON bloc_has_produit.produit_id = produit.id " +
    "LEFT JOIN bloc ON bloc.id = bloc_has_produit.bloc_id " +
    "WHERE projet_id =" +
    mysql.escape(urlproj);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
};

// Update a project with idProjet
exports.update = (req, res) => {
  let url = req.params.idProjet;
  let sql =
    "UPDATE projet SET nom=" +
    mysql.escape(req.body.nom) +
    ", remise=" +
    mysql.escape(req.body.remise) +
    ", prix_final=" +
    mysql.escape(req.body.prix) +
    ", adresse=" +
    mysql.escape(req.body.adresse) +
    ", etat_id=" +
    mysql.escape(req.body.etat) +
    ", distance_id=" +
    mysql.escape(req.body.distance) +
    ", CCTP_id=" +
    mysql.escape(req.body.cctp) +
    " WHERE id=" +
    mysql.escape(url);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
  });
};

// Delete a project with idProjet
exports.delete = (req, res) => {
  console.log("projet supprimé");
  let url = req.params.idProjet;
  let sql = "DELETE FROM projet WHERE id=" + mysql.escape(url);
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
  });
};

// Retrieve and return last created project id.
exports.findLastOne = (req, res) => {
  let sql = "SELECT MAX(id) as id FROM projet";
  bddsql.BDDSQL.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
};
