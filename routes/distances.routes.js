module.exports = (app) => {
  const distances = require("../controllers/distances.controller.js");

  // Retrieve all Distances
  app.get("/api/distances", distances.findAll);
};
